# CantorSet with Processing

A simple visualization of the Cantor Set written in Processing.

![](https://gitlab.com/linde9821/cantorset-with-processing/-/raw/master/CantorSet.gif)