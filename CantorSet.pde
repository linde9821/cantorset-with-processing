int lineHight = 20;
double minWidth;
int currentY = 60;

int currentIterrator = -1;
boolean forward = true;

boolean done = false;

void setup() {
  smooth();
  size(1500, 500);
  background(0);
}

void draw() {
  if (forward) {
    incrementSet();
  } else {
    decrementSet();
  }
  delay(350);

  if (!done) {
    saveFrame();
  }
}

void incrementSet() {
  minWidth = (1d / pow(3, currentIterrator)) * (1450d-50d) * (1d/3d);
  minWidth *= 0.98d;

  if (currentIterrator < 6) {
    forward = true;
    currentIterrator++;
  } else {
    forward = false;
  }

  drawLine(50, 1450);
}

void decrementSet() {
  minWidth = (1d / pow(3, (currentIterrator - 1))) * (1450d-50d) * (1d/3d);
  minWidth *= 1.1d;

  if (currentIterrator >= 0) {
    forward = false;
    currentIterrator--;
  } else {
    forward = true;
    done = true;
  }
  background(0);
  drawLine(50, 1450);
}

void drawLine(double begin, double end) {
  double lineWidth = end - begin;

  if (lineWidth <= minWidth) {
    return;
  }

  rect(roundDouble(begin), currentY, roundDouble(lineWidth), lineHight); 

  double thiredLineWidth = lineWidth * (1d/3d);

  double end1 = begin + thiredLineWidth;
  double begin2 = begin + thiredLineWidth * 2;
  double end2 = begin2 + thiredLineWidth;

  currentY += (3 * lineHight);

  drawLine(begin, end1);
  drawLine(begin2, end2);


  currentY -= (3 * lineHight);
} 

int roundDouble(double valueToRound) {
  int integerPart = (int) valueToRound;

  valueToRound = valueToRound - integerPart;

  if (valueToRound >= 0.5d)
    return integerPart + 1;

  return integerPart;
}
